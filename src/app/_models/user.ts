import { Photo } from './photo';

export interface User {
    id:number;
    age:number;
    user:string;
    knownAs:string;
    gender:string;
    lastActive:Date;
    photoUrl:string;
    city:string;
    country:string;
    interests?:string;
    introduction?:string;
    lookingFor?:string;
    created:Date;
    photos?:Photo[];
}
